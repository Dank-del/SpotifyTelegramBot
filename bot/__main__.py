import os

from telegram.ext import Dispatcher, ConversationHandler, InlineQueryHandler, CommandHandler, MessageHandler, Filters

from bot.helpers import create_table
from bot import USERNAME, updater, config
from bot.handlers import link, getusername, cancel, inlinenow, nowplaying, sendhelp, start, unlink, relink, sstats


def setup_handlers(d: Dispatcher):
    link_handler = ConversationHandler(
        entry_points=[CommandHandler('link', link)],
        states={USERNAME: [MessageHandler(Filters.text, getusername)]},
        fallbacks=[CommandHandler('cancel', cancel)])

    d.add_handler(link_handler)
    d.add_handler(InlineQueryHandler(inlinenow))
    d.add_handler(CommandHandler('now', nowplaying))
    d.add_handler(CommandHandler('help', sendhelp))
    d.add_handler(CommandHandler('start', start))
    d.add_handler(CommandHandler('unlink', unlink))
    d.add_handler(CommandHandler('relink', relink))
    d.add_handler(CommandHandler('sstats', sstats))


if __name__ == "__main__":
    if not os.path.exists('{}.db'.format(config.DB_NAME)):
        create_table()

    dispatcher = updater.dispatcher
    setup_handlers(dispatcher)
    updater.start_polling()
    updater.idle()

