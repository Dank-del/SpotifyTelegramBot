import requests
from bot import config
from bot.helpers import sql
import datetime
from io import BytesIO
from PIL import Image, ImageDraw, ImageFont


def getpic(r, uid, context):
    """retrives and passes all arguments to the image processor"""
    username = list(sql.get_user(uid)[0])[1]
    songname = r['item']['name']
    albumname = r['item']['album']['name']
    totaltime = r['item']['duration_ms']
    crrnttime = r['progress_ms']
    coverart = requests.get(r['item']['album']['images'][1]['url'])
    artists = ', '.join(x['name'] for x in r['item']['artists'])
    try:
        pfp = context.bot.getUserProfilePhotos(uid, limit=1)['photos'][0][0]['file_id']
        user = requests.get(context.bot.getFile(pfp).file_path)
    except:
        user = requests.get('https://files.catbox.moe/jp6szj.jpg')
    return process(username, songname, albumname, artists, crrnttime, totaltime, user, coverart)


def code(text):
    """get authtoken from a temporary json given by the webserver"""
    jkey = config.JSON_BLOB_KEY
    tempjson = requests.get('https://jsonblob.com/api/' + jkey).json()
    code = tempjson[text[7:]]
    keycount = len(tempjson.keys())
    if keycount > 10:
        for x in range(5):
            tempjson.pop(list(tempjson.keys())[x])
    requests.put('https://jsonblob.com/api/' + jkey, json=tempjson)
    return code


# process and format all info into an image
def process(name, song, album, artist, current, total, user, cover):
    print(f'{name} is listening to {song} by {artist} on {album}.')

    # input
    user = Image.open(BytesIO(user.content))
    cover = Image.open(BytesIO(cover.content))
    stotal = str(datetime.timedelta(milliseconds=total))
    stotal = stotal[3:7] if stotal[2] == '0' else stotal[2:7]
    scurrent = str(datetime.timedelta(milliseconds=current))
    scurrent = scurrent[3:7] if scurrent[2] == '0' else scurrent[2:7]

    # background
    image = Image.new('RGB', (600, 300), (31, 31, 39))
    draw = ImageDraw.Draw(image)
    draw.rectangle((0, 118) + image.size, (18, 20, 22))

    # images
    user.thumbnail((80, 80), Image.ANTIALIAS)
    image.paste(user, (52, 20))
    cover.thumbnail((128, 128), Image.ANTIALIAS)
    image.paste(cover, (28, 146))

    # fonts
    namefont = ImageFont.truetype('Fonts\\Montserrat-SemiBold.ttf', 32)
    if song != str(song.encode('utf-8'))[2:-1]:
        songfont = ImageFont.truetype('Fonts\\arial-unicode-ms.ttf', 29)
    else:
        songfont = ImageFont.truetype('Fonts\\Montserrat-SemiBold.ttf', 29)
    if (
            album != str(album.encode('utf-8'))[2:-1]
            or artist != str(artist.encode('utf-8'))[2:-1]
    ):
        infofont = ImageFont.truetype('Fonts\\arial-unicode-ms.ttf', 26)
    else:
        infofont = ImageFont.truetype('Fonts\\OpenSansCondensed-Bold.ttf', 26)
    timefont = ImageFont.truetype('Fonts\\Roboto-Medium.ttf', 17)

    # texts
    draw.text((184, 21), name + '\nis now listening to', fill=(255, 255, 255), font=namefont)
    draw.text((184, 138), truncate(song, songfont, 380), fill=(255, 255, 255), font=songfont)
    draw.text((184, 174), 'by ' + truncate(artist, infofont, 348), fill=(255, 255, 255), font=infofont)
    draw.text((184, 207), 'on ' + truncate(album, infofont, 348), fill=(255, 255, 255), font=infofont)

    # progress
    draw.rectangle((568, 251) + (185, 254), (255, 255, 255))
    draw.rectangle((185 + (current / total * 383), 251) + (185, 254), (131, 131, 132))
    draw.text((185, 258), str(scurrent), fill=(255, 255, 255), font=timefont)
    draw.text((534, 258), str(stotal), fill=(255, 255, 255), font=timefont)

    # output
    now = BytesIO()
    image.save(now, 'JPEG', quality=200)
    now.seek(0)
    return now


# shorten extra long text
def truncate(text, font, limit):
    edited = font.getsize(text)[0] > limit
    while font.getsize(text)[0] > limit: text = text[:-1]
    if edited:
        return text.strip() + '..'
    else:
        return text.strip()
