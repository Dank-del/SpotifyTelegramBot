import logging
from configparser import ConfigParser, SectionProxy
from urllib.parse import quote_plus as linkparse

# enable logging
from telegram.ext import Updater

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO)

LOG = logging.getLogger(__name__)

parser = ConfigParser()
parser.read("config.ini")
bot_config = parser["bot"]


class BotConfig:
    """
    Config class.
    Parses the config.ini in root directory and stores them in the class.
    """

    def __init__(self, p: SectionProxy):
        self.parser = p
        self.TOKEN = self.parser.get("token")
        self.DUMP_CHANNEL = self.parser.get("DUMP_CHANNEL")
        self.JSON_BLOB_KEY = self.parser.get("JSON_BLOB_KEY")
        self.CLIENT_ID = self.parser.get("CLIENT_ID")
        self.CLIENT_SECRET = self.parser.get("CLIENT_SECRET")
        self.REDIRECT_URL = self.parser.get("REDIRECT_URL")
        self.SUDO_LIST = self.parser.get("SUDO_LIST").split()
        self.SUDO_LIST = list(map(str, self.SUDO_LIST))
        self.DB_NAME = self.parser.get("DB_NAME", "bot")
        self.AUTH_LINK = f"https://accounts.spotify.com/authorize?client_id={self.CLIENT_ID}&response_type=code" \
                         f"&redirect_uri={linkparse(self.REDIRECT_URL)}&scope=user-read-currently-playing "


USERNAME, AUTHTOKEN = range(2)
config = BotConfig(bot_config)
updater = Updater(config.TOKEN, use_context=True)
